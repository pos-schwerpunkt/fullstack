package at.itkolleg.mobileappws.userservice;

import at.itkolleg.mobileappws.ui.model.request.UserDetailsRequestModel;
import at.itkolleg.mobileappws.ui.model.response.UserRest;

public interface UserService {
    UserRest createUser(UserDetailsRequestModel userDetails);
}
