package at.itkolleg.mobileappws.ui.model.response;

public class UserRest {
    private String firstName, lastName, eMail, userID;

    public UserRest() {
    }

    public UserRest(String firstName, String lastName, String eMail, String userID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.eMail = eMail;
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
