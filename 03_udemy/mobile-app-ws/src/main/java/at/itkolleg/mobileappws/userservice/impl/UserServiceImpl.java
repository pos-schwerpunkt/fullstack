package at.itkolleg.mobileappws.userservice.impl;

import at.itkolleg.mobileappws.shared.Utils;
import at.itkolleg.mobileappws.ui.model.request.UserDetailsRequestModel;
import at.itkolleg.mobileappws.ui.model.response.UserRest;
import at.itkolleg.mobileappws.userservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    Map<String, UserRest> users;
    Utils utils;

    public UserServiceImpl() {
    }

    @Autowired
    public UserServiceImpl(Utils utils) {
        this.utils = utils;
    }

    @Override
    public UserRest createUser(UserDetailsRequestModel userDetails) {

        UserRest returnValue = new UserRest();
        returnValue.seteMail(userDetails.getEmail());
        returnValue.setFirstName(userDetails.getFirstName());
        returnValue.setLastName(userDetails.getLastName());

        String userId = utils.generateUserId();
        returnValue.setUserID(userId);

        if (users == null) users = new HashMap<>();
        users.put(userId, returnValue);

        return returnValue;

    }

}
