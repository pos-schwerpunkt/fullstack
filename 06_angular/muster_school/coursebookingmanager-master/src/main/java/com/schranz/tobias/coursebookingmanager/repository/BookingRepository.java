package com.schranz.tobias.coursebookingmanager.repository;

import com.schranz.tobias.coursebookingmanager.model.Booking;
import com.schranz.tobias.coursebookingmanager.model.Course;
import com.schranz.tobias.coursebookingmanager.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    //List<Booking> findAllByStudent(Student student);

    Integer countBookingsByStudent(Student student);

    Integer countBookingsByCourse(Course course);
}
