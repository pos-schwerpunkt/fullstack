package com.schranz.tobias.coursebookingmanager.service;

import com.schranz.tobias.coursebookingmanager.model.Student;
import com.schranz.tobias.coursebookingmanager.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StudentService {

    @Autowired
    private StudentRepository repo;

    public List<Student> getAll(){
        return repo.findAll();
    }

    public Student save(Student student){

        return repo.save(student);
    }

    public Student get(Long id){

        return repo.findById(id).get();
    }

    public void delete(Long id){
        repo.deleteById(id);
    }

    public Student update(Long id, Student student){
        Student oldStudent = repo.findById(id).get();
        oldStudent.setFirstName(student.getFirstName());
        oldStudent.setLastName(student.getLastName());
        oldStudent.setBirthDate(student.getBirthDate());

        return repo.save(oldStudent);
    }
}
