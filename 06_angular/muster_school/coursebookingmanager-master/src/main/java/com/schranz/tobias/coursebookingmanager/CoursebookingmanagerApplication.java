package com.schranz.tobias.coursebookingmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CoursebookingmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoursebookingmanagerApplication.class, args);
    }

}
