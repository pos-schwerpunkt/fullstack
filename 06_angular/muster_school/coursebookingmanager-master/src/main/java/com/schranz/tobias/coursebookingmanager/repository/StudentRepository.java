package com.schranz.tobias.coursebookingmanager.repository;

import com.schranz.tobias.coursebookingmanager.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

}
