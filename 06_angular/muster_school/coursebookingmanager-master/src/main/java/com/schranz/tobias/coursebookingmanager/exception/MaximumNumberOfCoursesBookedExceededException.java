package com.schranz.tobias.coursebookingmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class MaximumNumberOfCoursesBookedExceededException extends RuntimeException{

    public MaximumNumberOfCoursesBookedExceededException() {
        super("Dieser Schüler hat die maximale Anzahl an buchbaren Kursen überschritten");
    }
}
