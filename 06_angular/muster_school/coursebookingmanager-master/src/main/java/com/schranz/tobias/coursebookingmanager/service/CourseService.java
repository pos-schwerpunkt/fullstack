package com.schranz.tobias.coursebookingmanager.service;

import com.schranz.tobias.coursebookingmanager.model.Course;
import com.schranz.tobias.coursebookingmanager.model.Student;
import com.schranz.tobias.coursebookingmanager.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
    @Autowired
    private CourseRepository repo;

    public List<Course> getAll(){

        return repo.findAll();
    }

    public Course save(Course course){

       return repo.save(course);
    }

    public Course get(Long id){

        return repo.findById(id).get();
    }

    public void delete(Long id){
        repo.deleteById(id);
    }

    public Course update(Long id, Course course){
        Course oldCourse = repo.findById(id).get();
        oldCourse.setDescription(course.getDescription());
        oldCourse.setEntryDeadline(course.getEntryDeadline());
        oldCourse.setName(course.getName());
        oldCourse.setMaxNumberOfParticipants(course.getMaxNumberOfParticipants());

        return repo.save(oldCourse);
    }

}
