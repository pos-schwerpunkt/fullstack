package com.schranz.tobias.coursebookingmanager.controller;

import com.schranz.tobias.coursebookingmanager.model.Booking;
import com.schranz.tobias.coursebookingmanager.model.Course;
import com.schranz.tobias.coursebookingmanager.model.Student;
import com.schranz.tobias.coursebookingmanager.service.BookingService;
import com.schranz.tobias.coursebookingmanager.service.CourseService;
import com.schranz.tobias.coursebookingmanager.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    private final String VERSION = "v1";
    private final String BASE_PATH = "/students/";

    @PostMapping(VERSION + BASE_PATH)
    public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student) {

        return ResponseEntity.ok(studentService.save(student));
    }

    @GetMapping(VERSION + BASE_PATH)
    public List<Student> getAllStudents() {
        return studentService.getAll();
    }

    @GetMapping(VERSION + BASE_PATH + "{id}")
    public Student getStudent(@PathVariable Long id){
        return studentService.get(id);
    }

    @DeleteMapping(VERSION + BASE_PATH + "{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable Long id) {

        studentService.delete(id);

        return ResponseEntity.ok("Student successfully deleted");
    }

    @PutMapping(VERSION + BASE_PATH + "{id}")
    public Student updateStudent(@PathVariable Long id, @Valid @RequestBody Student student) {

        return studentService.update(id, student);
    }

}
