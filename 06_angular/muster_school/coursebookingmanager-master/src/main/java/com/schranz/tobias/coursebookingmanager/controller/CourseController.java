package com.schranz.tobias.coursebookingmanager.controller;

import com.schranz.tobias.coursebookingmanager.model.Course;
import com.schranz.tobias.coursebookingmanager.model.Student;
import com.schranz.tobias.coursebookingmanager.service.CourseService;
import com.schranz.tobias.coursebookingmanager.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CourseController {

    @Autowired
    CourseService courseService;

    private final String VERSION = "v1";
    private final String BASE_PATH = "/courses/";

    @PostMapping(VERSION + BASE_PATH)
    public Course createCourse(@Valid @RequestBody Course course) {

        return courseService.save(course);
    }

    @GetMapping(VERSION + BASE_PATH)
    public List<Course> getAllCourses() {

        return courseService.getAll();
    }

    @GetMapping(VERSION + BASE_PATH + "{id}")
    public Course getCourse(@PathVariable Long id){
        return courseService.get(id);
    }

    @DeleteMapping(VERSION + BASE_PATH + "{id}")
    public ResponseEntity<String> deleteCourse(@PathVariable Long id) {

        courseService.delete(id);

        return ResponseEntity.ok("Course successfully deleted");
    }

    @PutMapping(VERSION + BASE_PATH + "{id}")
    public Course updateCourse(@PathVariable Long id, @Valid @RequestBody Course course) {

       return courseService.update(id, course);
    }

}
