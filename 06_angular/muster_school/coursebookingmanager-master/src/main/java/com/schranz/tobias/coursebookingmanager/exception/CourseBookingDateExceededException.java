package com.schranz.tobias.coursebookingmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class CourseBookingDateExceededException extends RuntimeException{

    public CourseBookingDateExceededException() {
        super("Anmeldeschluss erreicht! In diesen Kurs darf sich nicht mehr eingeschrieben werden.");
    }
}
