package com.schranz.tobias.coursebookingmanager.controller;

import com.schranz.tobias.coursebookingmanager.model.Booking;
import com.schranz.tobias.coursebookingmanager.model.Course;
import com.schranz.tobias.coursebookingmanager.model.Student;
import com.schranz.tobias.coursebookingmanager.service.BookingService;
import com.schranz.tobias.coursebookingmanager.service.CourseService;
import com.schranz.tobias.coursebookingmanager.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class BookingController {

    @Autowired
    BookingService bookingService;

    @Autowired
    StudentService studentService;

    @Autowired
    CourseService courseService;

    private final String VERSION = "v1";
    private final String BASE_PATH = "/bookings/";

    @PostMapping(VERSION + "/students/{studentId}/courses/{courseId}")
    public Booking createBooking(@PathVariable Long studentId, @PathVariable Long courseId) {

        Student student = studentService.get(studentId);
        Course course = courseService.get(courseId);

        return bookingService.save(student, course);
    }

    @DeleteMapping(VERSION + BASE_PATH + "{id}")
    public ResponseEntity<String> deleteBooking(@PathVariable Long id) {

        bookingService.delete(id);

        return ResponseEntity.ok("Booking successfully deleted");
    }

    @GetMapping(VERSION + BASE_PATH)
    public List<Booking> getAllBookings(){
        return bookingService.getAll();
    }
}
