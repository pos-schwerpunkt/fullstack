package com.schranz.tobias.coursebookingmanager.service;

import com.schranz.tobias.coursebookingmanager.exception.CourseBookingDateExceededException;
import com.schranz.tobias.coursebookingmanager.exception.CourseFullException;
import com.schranz.tobias.coursebookingmanager.exception.MaximumNumberOfCoursesBookedExceededException;
import com.schranz.tobias.coursebookingmanager.exception.ResourceNotFoundException;
import com.schranz.tobias.coursebookingmanager.model.Booking;
import com.schranz.tobias.coursebookingmanager.model.Course;
import com.schranz.tobias.coursebookingmanager.model.Student;
import com.schranz.tobias.coursebookingmanager.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    private BookingRepository repo;

    public Booking save(Student student, Course course){

        if(getNumberOfBookingByStudent(student) > 6){
            throw new MaximumNumberOfCoursesBookedExceededException();
        }

        if(!isCourseBookable(course)){
            throw new CourseBookingDateExceededException();
        }
        if(!isCourseFree(course)){
            throw new CourseFullException();
        }

        Booking booking = new Booking();
        booking.setStudent(student);
        booking.setCourse(course);

        return repo.save(booking);
    }

    public List<Booking> getAll(){

        if(repo.findAll().isEmpty()){
            throw new ResourceNotFoundException("Es existieren keine Buchungen.");
        }
        return repo.findAll();
    }

    public void delete(Long id){
        repo.deleteById(id);
    }

    public Booking get(Long id){

        return repo.findById(id).get();
    }

    public Integer getNumberOfBookingByStudent(Student student){
        return repo.countBookingsByStudent(student);
    }

    private boolean isCourseFree(Course course){
         return (repo.countBookingsByCourse(course) < course.getMaxNumberOfParticipants());
    }

    private boolean isCourseBookable(Course course){
        Date today = new Date();
        return (course.getEntryDeadline().compareTo(today) > 0);
    }
}
