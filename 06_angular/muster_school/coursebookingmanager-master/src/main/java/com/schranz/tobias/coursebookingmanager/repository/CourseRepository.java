package com.schranz.tobias.coursebookingmanager.repository;

import com.schranz.tobias.coursebookingmanager.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

}
