package com.schranz.tobias.coursebookingmanager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class CourseFullException extends RuntimeException{

    public CourseFullException() {
        super("Dieser Kurs ist bereits voll! Es darf sich nicht mehr eingeschrieben werden.");
    }
}
