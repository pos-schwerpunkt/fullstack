import { Component, OnInit } from '@angular/core';
import {Kurs} from '../model/kurs';
import {RestService} from '../rest.service';

@Component({
  selector: 'kurs-list',
  templateUrl: './kurs-list.component.html',
  styleUrls: ['./kurs-list.component.css']
})
export class KursListComponent implements OnInit {

  k: Kurs[]=[];

  constructor(private restAPI: RestService) { }

  ngOnInit() {
    this.restAPI.getAllKurs().subscribe({
      next: k => {
      this.k = k
      }
    })
    
  }
}
