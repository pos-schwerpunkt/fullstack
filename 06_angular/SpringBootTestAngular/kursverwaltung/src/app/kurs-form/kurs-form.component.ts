import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Kurs} from '../model/kurs';
import {KursSaveDto} from '../model/kurs-save-dto';
import {RestService} from '../rest.service' 

@Component({
  selector: 'app-kurs-form',
  templateUrl: './kurs-form.component.html',
  styleUrls: ['./kurs-form.component.css']
})
export class KursFormComponent implements OnInit {

  k: Kurs;
  dto: KursSaveDto;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private restAPI: RestService) {
    }

  ngOnInit(): void {
  }

  onSubmit() {
    //const neuerKurs = new Kurs(null, this.dto.name, null, false);
  
    this.restAPI.addNewKurs(this.k).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['kurs']);
  }

}
