import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { KursListComponent } from './kurs-list/kurs-list.component';
import { KursFormComponent } from './kurs-form/kurs-form.component';
import { RestService } from './rest.service';

@NgModule({
  declarations: [
    AppComponent,
    KursListComponent,
    KursFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	    HttpClientModule,
	    FormsModule
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
