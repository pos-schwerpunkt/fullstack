import { Component } from '@angular/core';
import {RestService} from './rest.service';
import {Kurs} from './model/kurs';
import {KursSaveDto} from './model/kurs-save-dto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string;

  constructor() {
    this.title = 'Kursverwaltung';
  }
}
