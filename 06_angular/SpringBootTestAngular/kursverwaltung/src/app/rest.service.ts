import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import {Kurs} from './model/kurs';
import {KursSaveDto} from './model/kurs-save-dto';
import { Observable } from 'rxjs/Observable';
import { catchError, tap, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  private kursURL: string = 'http://localhost:8080/kurs';

  constructor(private http: HttpClient) {
    //this.kursURL = 'http://localhost:8080/kurs';
  }

  public getAllKurs(): Observable<Kurs[]> {
    return this.http.get<Kurs[]>(this.kursURL)
    .pipe(
      tap(data =>
      console.log('All: '+ JSON.stringify(data))));
  }


  public addNewKurs(k: Kurs) {
     return this.http.post<Kurs>(this.kursURL, k);
  }
}
