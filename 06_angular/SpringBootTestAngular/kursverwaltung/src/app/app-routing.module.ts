import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {KursListComponent} from './kurs-list/kurs-list.component';
import {KursFormComponent} from './kurs-form/kurs-form.component';

const routes: Routes = [
    { path: '', component: KursListComponent },
    { path: 'kurs', component: KursFormComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }