package at.itkolleg.test.exceptions;

public class KursleiterNotFoundException extends Exception {
    public KursleiterNotFoundException() {
        super("Der Kursleiter wurde nicht gefunden.");
    }
}
