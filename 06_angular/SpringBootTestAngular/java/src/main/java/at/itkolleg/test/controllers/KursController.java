package at.itkolleg.test.controllers;

import at.itkolleg.test.entities.Kurs;
import at.itkolleg.test.exceptions.KursNotFoundException;
import at.itkolleg.test.services.KursService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
//@RequestMapping("/api/v1")
public class KursController {

    private final KursService service;
    private final String KURS = "/kurs";
    private final String KURSID = "/kurs/{id}";

    public KursController(KursService service) {
        this.service = service;
    }

    @GetMapping(KURS)
    public ResponseEntity<List<Kurs>> getAllKurse() {
        return ResponseEntity.ok(service.getAllCourses());
    }

//    @GetMapping(KURS)
//    public List<Kurs> getAllAsList() {
//        return service.getAllCourses();
//    }

    @GetMapping(KURSID)
    public ResponseEntity<Kurs> getKursById(@PathVariable Long id) throws KursNotFoundException {
        Kurs k = service.getKursById(id);
        return ResponseEntity.ok().body(k);
    }

//    @GetMapping(KURSID)
//    public ResponseEntity<Kurs> getKursById(@PathVariable Long id) throws KursNotFoundException {
//        return new ResponseEntity<>(service.getKursById(id), HttpStatus.OK);
//    }

//    @PostMapping(KURS)
//    public ResponseEntity<Kurs> addNewKurs(@Valid @RequestBody Kurs k) throws URISyntaxException {
//        Kurs newKurs = service.addNewKurs(k);
//        return ResponseEntity.created(new URI(KURS + newKurs.getId())).body(k);
//    }

    @PostMapping(KURS)
    public Kurs addNewKurs(@Valid @RequestBody Kurs k) {
        System.out.println(k.getId());
        return service.addNewKurs(k);
    }

    @DeleteMapping(KURSID)
    public ResponseEntity<Kurs> deleteKurs(@PathVariable Long id) {
        service.deleteKurs(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(KURSID)
    public ResponseEntity<Kurs> updateKurs(@PathVariable Long id, @Valid @RequestBody Kurs k) throws KursNotFoundException {
        service.updateKurs(id, k);
        return ResponseEntity.ok(k);
    }

}
