package at.itkolleg.test.exceptions;

public class KursNotFoundException extends Exception {
    public KursNotFoundException() {
        super("Der Kurs wurde nicht gefunden.");
    }
}
