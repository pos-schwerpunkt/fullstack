package at.itkolleg.test.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class Kursleiter {

    public Kursleiter() {
    }

    @Id
    @GeneratedValue
    @Column(name = "leiter_id")
    private Long id;
    private String name;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "kurs_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Kurs kurse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Kurs getKurse() {
        return kurse;
    }

    public void setKurse(Kurs kurse) {
        this.kurse = kurse;
    }
}
