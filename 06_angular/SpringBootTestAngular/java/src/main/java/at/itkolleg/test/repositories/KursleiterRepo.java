package at.itkolleg.test.repositories;

import at.itkolleg.test.entities.Kurs;
import at.itkolleg.test.entities.Kursleiter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KursleiterRepo extends JpaRepository<Kursleiter, Long> {
    public List<Kursleiter> getKursleiterByKurse(Kurs k);
}
