package at.itkolleg.test.exceptions;

public class KursNotBookedException extends Exception {
    public KursNotBookedException() {
        super("Dieser Kurs wurde noch nicht gebucht, daher kann kein Kursleiter eingetragen werden.");
    }
}
