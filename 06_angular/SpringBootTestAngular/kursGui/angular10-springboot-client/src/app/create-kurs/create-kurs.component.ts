import { Component, OnInit } from '@angular/core';
import {KursService} from '../kurs.service';
import {Kurs} from '../model/kurs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-kurs',
  templateUrl: './create-kurs.component.html',
  styleUrls: ['./create-kurs.component.css']
})
export class CreateKursComponent implements OnInit {
  kurs: Kurs = new Kurs();
  submitted = false;

  constructor(private service: KursService,
    private router: Router) { }

  ngOnInit() {
  }

  newKurs(): void {
    this.submitted = false;
    this.kurs = new Kurs();
  }

  save() {
    this.service.createKurs(this.kurs)
    .subscribe(data => console.log(data), error => console.log(error));
    this.kurs = new Kurs();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['kurs']);
  }

}
