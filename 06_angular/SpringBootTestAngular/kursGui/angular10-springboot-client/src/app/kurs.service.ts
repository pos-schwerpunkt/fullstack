import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Kurs } from './model/kurs';

@Injectable({
  providedIn: 'root'
})
export class KursService {
  private baseUrl = 'http://localhost:8080/kurs';

  constructor(private http: HttpClient) { }

  getKurs(id: number): Observable<Kurs> {
    return this.http.get<Kurs>(`${this.baseUrl}/${id}`);
  }

  createKurs(kurs: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, kurs);
  }

  updateKurs(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteKurs(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getKursList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }


}
