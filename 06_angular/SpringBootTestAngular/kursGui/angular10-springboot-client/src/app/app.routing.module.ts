import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {KursDetailsComponent} from './kurs-details/kurs-details.component';
import {CreateKursComponent} from './create-kurs/create-kurs.component';
import {KursListComponent} from './kurs-list/kurs-list.component';
import {UpdateKursComponent} from './update-kurs/update-kurs.component';

const routes: Routes = [
    { path: '', redirectTo: 'kurs', pathMatch: 'full' },
    { path: 'kurs', component: KursListComponent },
    { path: 'add', component: CreateKursComponent },
    { path: 'update/:id', component: UpdateKursComponent },
    { path: 'details/:id', component: KursDetailsComponent },
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }