import { Component, OnInit } from '@angular/core';
import {KursDetailsComponent} from '../kurs-details/kurs-details.component';
import {KursService} from '../kurs.service';
import { Observable } from "rxjs";
import {Kurs} from '../model/kurs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-kurs-list',
  templateUrl: './kurs-list.component.html',
  styleUrls: ['./kurs-list.component.css']
})
export class KursListComponent implements OnInit {
  kurs: Observable<Kurs[]>;

  constructor(private service: KursService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.kurs = this.service.getKursList();
  }
  
  deleteKurs(id: number) {
    this.service.deleteKurs(id).subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error)
    );
  }

  kursDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  updateKurs(id: number) {
    this.router.navigate(['update', id]);
  }

}
