import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CreateKursComponent } from './create-kurs/create-kurs.component';
import { KursDetailsComponent } from './kurs-details/kurs-details.component';
import { KursListComponent } from './kurs-list/kurs-list.component';
import { UpdateKursComponent } from './update-kurs/update-kurs.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateKursComponent,
    KursDetailsComponent,
    KursListComponent,
    UpdateKursComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
