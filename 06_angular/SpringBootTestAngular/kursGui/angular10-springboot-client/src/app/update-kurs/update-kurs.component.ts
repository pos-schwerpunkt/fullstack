import { Component, OnInit } from '@angular/core';
import{Kurs} from '../model/kurs';
import {KursService} from '../kurs.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update-kurs',
  templateUrl: './update-kurs.component.html',
  styleUrls: ['./update-kurs.component.css']
})
export class UpdateKursComponent implements OnInit {
  id: number;
  submitted: false;
  kurs: Kurs;

  constructor(private route: ActivatedRoute,
    private router: Router, 
    private service: KursService) { }

  ngOnInit() {
    this.kurs = new Kurs();
    this.submitted = false;
    this.id = this.route.snapshot.params['id'];
    this.service.getKurs(this.id) .subscribe(data => {
      console.log(data)
      this.kurs = data;
    }, error => console.log(error));
}

  updateKurs() {
      this.service.updateKurs(this.id, this.kurs)
      .subscribe(data => console.log(data), error => console.log(error));
        this.kurs = new Kurs();
        this.gotoList();
  }
  
  onSubmit() {
    this.updateKurs();  
  }

  gotoList() {
    this.router.navigate(['/kurs']);
  }
}
