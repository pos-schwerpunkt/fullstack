import { Component, OnInit } from '@angular/core';
import {Kurs} from '../model/kurs';
import {KursService} from '../kurs.service';
import {KursListComponent} from '../kurs-list/kurs-list.component';
import { Router, ActivatedRoute } from '@angular/router';
//import { serialize } from 'v8';

@Component({
  selector: 'app-kurs-details',
  templateUrl: './kurs-details.component.html',
  styleUrls: ['./kurs-details.component.css']
})
export class KursDetailsComponent implements OnInit {
  id: number;
  kurs: Kurs;

  constructor(private route: ActivatedRoute, private router: Router,
    private service: KursService) { }

  ngOnInit() {
    this.kurs = new Kurs();
    this.id = this.route.snapshot.params['id'];
    this.service.getKurs(this.id)
    .subscribe(data => {
      console.log(data)
      this.kurs = data;
    }, error => console.log(error));
  }

  list(){
    this.router.navigate(['kurs']);
  }

}
