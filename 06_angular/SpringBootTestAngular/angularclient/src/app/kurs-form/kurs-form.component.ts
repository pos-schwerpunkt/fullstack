import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { KursService } from '../kurs-service.service';
import { kurs } from '../kurs';

@Component({
  selector: 'app-kurs-form',
  templateUrl: './kurs-form.component.html',
  styleUrls: ['./kurs-form.component.css']
})
export class KursFormComponent {

  kurs: kurs;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private kursService: KursService) {
      this.kurs = new kurs();
    }

  onSubmit() {
    this.kursService.save(this.kurs).subscribe(result => this.gotoKursList());
  }

  gotoKursList() {
    this.router.navigate(['/']);
  }

}
