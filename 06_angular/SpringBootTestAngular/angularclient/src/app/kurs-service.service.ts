import { Injectable } from '@angular/core';
	import { HttpClient, HttpHeaders } from '@angular/common/http';
	import { kurs } from './kurs';
  import { Observable } from 'rxjs/Observable';
	 
	@Injectable()
	export class KursService {
	 
	  private kursUrl: string;
	 
	  constructor(private http: HttpClient) {
	    this.kursUrl = 'http://localhost:8080/kurs';
	  }
	 
	  public findAll(): Observable<kurs[]> {
	    return this.http.get<kurs[]>(this.kursUrl);
	  }
	 
	  public save(kurse: kurs) {
	    return this.http.post<kurs>(this.kursUrl, kurse);
	  }
	}