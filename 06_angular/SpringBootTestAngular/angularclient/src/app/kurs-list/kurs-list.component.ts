import { Component, OnInit } from '@angular/core';
	import { kurs } from '../kurs';
	import { KursService } from '../kurs-service.service';
	 
	@Component({
	  selector: 'app-kurs-list',
	  templateUrl: './kurs-list.component.html',
	  styleUrls: ['./kurs-list.component.css']
	})
	export class KursListComponent implements OnInit {
	 
	  kurse: kurs[];
	 
	  constructor(private kursService: KursService) {
	  }
	 
	  ngOnInit() {
	    this.kursService.findAll().subscribe(data => {
	      this.kurse = data;
	    });
	  }
	}