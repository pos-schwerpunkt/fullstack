import {Component} from '@angular/core';
import {RestService} from './rest.service';
import {Student} from './models/student';
import {StudentSaveDto} from './models/studentSaveDto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pos';
  students: Student[];
  student: StudentSaveDto;
  street = '';

  constructor(
    private restService: RestService
  ) {
    this.student = new StudentSaveDto(0, '');
  }

  saveData() {
    this.restService.saveStudent(this.student).subscribe((data: any) => {
      console.log(data);
    });
  }

  loadData() {
    this.restService.getStudents().subscribe((data: Student[]) => {
      console.log(data);
      this.students = data;
    });
  }
}
