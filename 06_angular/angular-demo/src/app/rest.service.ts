import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StudentSaveDto} from './models/studentSaveDto';
import {Student} from './models/student';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private httpClient: HttpClient) {
  }

  getStudents() {
    return this.httpClient.get(`http://localhost:8080/api/schoolclass/1/students`);
  }

  saveStudent(studentSaveDto: StudentSaveDto) {
    console.log(studentSaveDto);

    const url = `http://localhost:8080/api/schoolclass/` + studentSaveDto.schoolclassId + `/addstudent`;

    const student = new Student(null, studentSaveDto.name, null);

    return this.httpClient.post<StudentSaveDto>(
      url,
      student
    );
  }
}
