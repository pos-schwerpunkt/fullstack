package at.itkolleg.FirstSpringBoot.controller;

import at.itkolleg.FirstSpringBoot.entities.Product;
import at.itkolleg.FirstSpringBoot.services.ProductJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductJPARepository productService;

    @GetMapping("/welcome")
    public String sayHi() {
        return "Welcome to product page";
    }

    @GetMapping("/product")
    public Product getProduct() {
        Product myProduct = new Product(1, "A", 10);
        return myProduct;
    }

    @GetMapping("/products")
    public List<Product> getProducts() {
//        List<Product> myProducts = new ArrayList<>();
//        Product p1 = new Product(1, "A", 10);
//        Product p2 = new Product(1, "B", 10);
//        Product p3 = new Product(1, "C", 10);
//        myProducts.add(p1);
//        myProducts.add(p2);
//        myProducts.add(p3);
//        return myProducts;

        return productService.findAll();
    }

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable int id) {
        return productService.findById(id).get();
    }

    @DeleteMapping("/products/{id}")
    public String deleteProductById(@PathVariable int id) {
        productService.deleteById(id);
        return "1";
    }

    @PostMapping("/products")
    public String addNewProduct(@RequestBody Product p) {
        productService.save(p);
        return "ok";
    }

}
