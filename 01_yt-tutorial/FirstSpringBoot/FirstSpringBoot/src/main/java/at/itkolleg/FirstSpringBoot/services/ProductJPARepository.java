package at.itkolleg.FirstSpringBoot.services;

import at.itkolleg.FirstSpringBoot.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductJPARepository extends JpaRepository<Product, Integer> {

}
