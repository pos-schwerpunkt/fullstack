package at.itkolleg.students.controller;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class CourseController {
    @Autowired
    private CourseService service;

   @GetMapping("/course")
    public ResponseEntity<List<Course>> getAll() {
       return ResponseEntity.ok(service.getAll());
   }

   @GetMapping("course/{id}")
    public ResponseEntity<Course> getCourseById(@PathVariable Long id) {
       return ResponseEntity.ok(service.getCourseById(id));
   }

   @PostMapping("/course")
    public ResponseEntity<Course> addNewCourse(@Valid @RequestBody Course course) throws URISyntaxException {
       Course c = service.addNewCourse(course);
       return ResponseEntity.created(new URI("/api/course" + c.getId())).body(c);
   }

   @PutMapping("/course/{id}")
    public ResponseEntity<Course> updateCourse(@Valid @RequestBody Course course, @PathVariable Long id) {
       service.updateCourse(id, course);
       return ResponseEntity.ok().build();
   }

   @DeleteMapping("/course/{id}")
    public ResponseEntity<Void> deleteCourse(@PathVariable Long id) {
       service.deleteCourseById(id);
       return ResponseEntity.ok().build();
   }
}
