package at.itkolleg.students.service;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.repository.CourseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepo repo;

    public List<Course> getAll() {
        return repo.findAll();
    }

    public Course getCourseById(Long id) {
        return repo.findById(id).get();
    }

    public Course addNewCourse(Course c) {
        return repo.save(c);
    }

    public void deleteCourseById(Long id) {
        repo.deleteById(id);
    }

    public Course updateCourse(Long id, Course c) {
        c.setId(id);
        return repo.save(c);
    }
}
