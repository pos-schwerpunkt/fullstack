package at.itkolleg.students.entity;

import javax.persistence.*;

@Entity
@Table(name = "COURSE")
public class Course {
    @Column(name = "course_id")
    private Long id;
    private String name;

   // private Set<Student> students;
    public Course() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

//    Wird dieses Datenfeld benötigt? Im Tutorial wird es eingebaut; führt bei mir allerdings zu einer Art Endlosschleife.
//    https://javabycode.com/sf/spring-boot-tutorial/spring-boot-jpa-one-to-many-relationship-mapping-example.html
//    Many-to-One Z. 46, One-to-Many Z. 44 in den jeweiligen Entities

//    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    public Set<Student> getStudents() {
//        return students;
//    }
//
//    public void setStudents(Set<Student> students) {
//        this.students = students;
//    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
