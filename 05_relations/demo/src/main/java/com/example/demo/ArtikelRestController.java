package com.example.demo;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArtikelRestController {

    private ArtikelRepo artikelRepo;

    public ArtikelRestController(ArtikelRepo artikelRepo) {
        this.artikelRepo = artikelRepo;
    }

    @PostMapping("/artikel")
    public void insertDummy()
    {
        artikelRepo.save(new Artikel("Retro Windows XP"));
        artikelRepo.save(new Artikel("Retro Windows Vista"));
        artikelRepo.save(new Artikel("Retro Windows Vista Ultimate"));

    }
}
