INSERT INTO TEACHER VALUES (1, 'Merlin', 'Bostock');
INSERT INTO TEACHER VALUES (2, 'Lina', 'Goff');
INSERT INTO TEACHER VALUES (3, 'Blake', 'Wyatt');

INSERT INTO COURSE values (1, 'Mathematik', 2);
INSERT INTO COURSE values (2, 'Deutsch', 3);
INSERT INTO COURSE values (3, 'Englisch', 1);

INSERT INTO STUDENT values (10001, 'John Doe', 'A123456EN', 3);
INSERT INTO STUDENT values (10004, 'John Doe2', 'A123456EX', 3);
INSERT INTO STUDENT values (10002, 'Max Mustermann', 'F87460EX', 1);
INSERT INTO STUDENT values (10003, 'Maria Bergfuchs', 'R10975UM', 2);

