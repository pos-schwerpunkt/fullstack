package at.itkolleg.students.controller;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.entity.Student;
import at.itkolleg.students.service.CourseService;
import at.itkolleg.students.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class StudentController {

    private final StudentService service;
    private final CourseService courseService;

    public StudentController(StudentService service, CourseService courseService) {
        this.service = service;
        this.courseService = courseService;
    }

    @GetMapping(value = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Student>> findAll() {
        return ResponseEntity.ok(service.getAllStudents());
    }

    @GetMapping("/students/course/{id}")
    public ResponseEntity<List<Student>> findAllByCourseId(@PathVariable Long id){
        Course course = courseService.getCourseById(id);
        List<Student> studentlist = service.getAllStudentsByCourse(course);
        return new ResponseEntity<List<Student>>(studentlist, HttpStatus.OK);
    }

    @GetMapping(value = "/students/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Student> findContactById(@PathVariable long id) {
        Student s = service.getStudentById(id);
        return ResponseEntity.ok(s);  // return 200, with json body
    }

    @PostMapping(value = "/students")
    public ResponseEntity<Student> addContact(@Valid @RequestBody Student s)
            throws URISyntaxException {
        Student newStudent = service.addNewStudent(s);
        return ResponseEntity.created(new URI("/api/students/" + newStudent.getId()))
                .body(s);
    }

    @DeleteMapping(path="/students/{id}")
    public ResponseEntity<Void> deleteContactById(@PathVariable long id) {
            service.deleteStudent(id);
            return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/students/{id}")
    public ResponseEntity<Student> updateContact(@Valid @RequestBody Student s,
                                                 @PathVariable long id) {
        service.update(id, s);
        return ResponseEntity.ok().build();
    }
}

//    @GetMapping("/students")
//    public List<Student> getAllStudents() {
//        return studentRepo.findAll();
//    }

//    @GetMapping("/students/{id}")
//    public Student getStudentById(@PathVariable Long id) {
//        return studentRepo.findById(id).get();
//    }

//    @PostMapping("/students")
//    public String addStudent(@RequestBody Student s) {
//        studentRepo.save(s);
//        return "added";
//    }

//    @DeleteMapping("/students/{id}")
//    public String deleteStudent(@PathVariable Long id) {
//        studentRepo.deleteById(id);
//        return "deleted";
//    }