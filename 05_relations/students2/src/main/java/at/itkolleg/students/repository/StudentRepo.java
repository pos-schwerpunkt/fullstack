package at.itkolleg.students.repository;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    List<Student> getStudentsByCourse(Course course);
}
