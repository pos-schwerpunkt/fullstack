package at.itkolleg.students.repository;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepo extends JpaRepository<Course, Long> {
}
