package at.itkolleg.students.controller;

import at.itkolleg.students.entity.Teacher;
import at.itkolleg.students.exceptions.TeacherNotFoundException;
import at.itkolleg.students.service.TeacherService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class TeacherController {

    private final TeacherService service;

    public TeacherController(TeacherService service) {
        this.service = service;
    }

    @GetMapping("/teachers")
    public ResponseEntity<List<Teacher>> getAllTeachers() {
        return ResponseEntity.ok(service.getAllTeacher());
    }

    @GetMapping("/teachers/{id}")
    public ResponseEntity<Teacher> getTeacherById(@PathVariable Long id) throws TeacherNotFoundException {
        return new ResponseEntity<>(service.getTeacherById(id), HttpStatus.OK);
    }

    @PostMapping("/teachers")
    public ResponseEntity<Teacher> addNewTeacher(@Valid @RequestBody Teacher teacher) throws URISyntaxException {
        Teacher newTeacher = service.addNewTeacher(teacher);
        return ResponseEntity.created(new URI("/teachers/" + newTeacher.getId())).body(teacher);
    }

    @DeleteMapping("/teachers/{id}")
    public ResponseEntity<Teacher> deleteTeacher(@PathVariable Long id) {
        service.deleteTeacherById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/teachers")
    public ResponseEntity<Teacher> updateTeacher(@Valid @RequestBody Teacher teacher) throws TeacherNotFoundException {
        service.updateTeacherById(teacher);
        return ResponseEntity.ok().build();
    }
}