package at.itkolleg.students.service;

import at.itkolleg.students.entity.Teacher;
import at.itkolleg.students.exceptions.TeacherNotFoundException;
import at.itkolleg.students.repository.TeacherRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    private TeacherRepo repo;

    public TeacherService(TeacherRepo repo) {
        this.repo = repo;
    }

    public Teacher addNewTeacher(Teacher teacher) {
        return repo.save(teacher);
    }

    public List<Teacher> getAllTeacher() {
        return repo.findAll();
    }

    public Teacher getTeacherById(Long id) throws TeacherNotFoundException {
        Optional<Teacher> teacherOptional = repo.findById(id);
        if(teacherOptional.isPresent()) {
            return teacherOptional.get();
        } else {
            throw new TeacherNotFoundException();
        }
    }

//    public List<Teacher> getAllTeachersByCourse(Course course) {
//        return repo.getTeacherByCourse(course);
//    }

    public void deleteTeacherById(Long id) {
        repo.deleteById(id);
    }

    public Teacher updateTeacherById(Teacher teacher) throws TeacherNotFoundException {
        Optional<Teacher> teacherOptional = repo.findById(teacher.getId());
        if(teacherOptional.isPresent()) {
            Teacher updatedTeacher = teacherOptional.get();
            updatedTeacher.setFirstName(teacher.getFirstName());
            updatedTeacher.setLastName(teacher.getLastName());
            return repo.save(updatedTeacher);
        } else {
            throw new TeacherNotFoundException();
        }
    }
}
