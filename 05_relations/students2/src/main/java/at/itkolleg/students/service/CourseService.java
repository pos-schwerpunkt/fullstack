package at.itkolleg.students.service;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.repository.CourseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    private CourseRepo repo;

    public CourseService(CourseRepo repo) {
        this.repo = repo;
    }

    public List<Course> getAll() {
        return repo.findAll();
    }

    public Course getCourseById(Long id) {
        return repo.findById(id).get();
    }

    public Course addNewCourse(Course c) {
        return repo.save(c);
    }

    public void deleteCourseById(Long id) {
        repo.deleteById(id);
    }

    public Course updateCourse(Long id, Course c) {
      /*  Optional<Course> courseOptional = repo.findById(c.getId());
        if(courseOptional.isPresent())
        {
            Course course = courseOptional.get();
        } else
        {
            throw new CourseNotFoundException();
        }*/
        c.setId(id);
        return repo.save(c);
    }
}
