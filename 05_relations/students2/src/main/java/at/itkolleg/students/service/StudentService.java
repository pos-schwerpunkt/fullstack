package at.itkolleg.students.service;

import at.itkolleg.students.entity.Course;
import at.itkolleg.students.entity.Student;
import at.itkolleg.students.repository.StudentRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepo repo;

    public StudentService(StudentRepo repo) {
        this.repo = repo;
    }

    public Student addNewStudent(Student student) {
        return repo.save(student);
    }

    public List<Student> getAllStudents() {
       return repo.findAll();
    }

    public List<Student> getAllStudentsByCourse(Course course) {
        return repo.getStudentsByCourse(course);
    }

    public Student getStudentById(Long id) {
        return repo.findById(id).get();
    }

    public void deleteStudent(Long id) {
        repo.deleteById(id);
    }

    public void update(Long id, Student s) {
        s.setId(id);
        repo.save(s);
    }
}
