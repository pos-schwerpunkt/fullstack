package at.itkolleg.students.exceptions;

public class TeacherNotFoundException extends Exception {
    public TeacherNotFoundException() {
        super("Teacher was not found!");
    }
}
