package at.itkolleg.vaadin.backend.repository;

import at.itkolleg.vaadin.backend.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
